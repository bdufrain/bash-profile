#!/bin/bash

currentPath=$(dirname -- "$(readlink -f -- "${BASH_SOURCE[0]}")")
source "${currentPath}/public/ads_helper"

if [ $(cat ~/.ads-cli.yaml | grep -c token) -eq 0 ];then
  echo "Please configure an ads-cli security token";
  return 1;
fi
# Populate a list of attrName to description
saveAttrTable

SEARCH_SPACE="";
projectionString="";
queryLimit="10000";
while getopts "Aha:fl:v:sn:" opt; do
case $opt in
  v) # Provide a view to query
    SEARCH_SPACE=${OPTARG}
    ;;
  n) # Lookup the view by viewName
    SEARCH_SPACE=$(adsGet View viewName | grep $OPTARG | awk '{print $2;exit}' | sed 's/[",]//g')
    ;;
  A) # Show prompt to specify attributes in the view
    limitAttrs="Y"
    ;;
  a) # Specify a view attribute to include in the query
    projectionString="$projectionString ${OPTARG}"
    ;;
  f) # Indicates that you would like to specify a filter on the query
    useOptions="Y"
    useFilter="Y"
    ;;
  s) # Indicates that you would like to specify a filter on the query
    useOptions="Y"
    useSort="Y"
    ;;
  l) # Indicates that you would like to specify a filter on the query
    useOptions="Y"
    queryLimit=${OPTARG}
    ;;
  h) # Display help.
    echo "$0 usage:" && grep " .)\ #" $0
    exit 1
    ;;
  *)
    #print option error
    echo "Invalid option: $OPTARG" 1>&2
    exit 1
    ;;
  :)
  #print argument error
  echo "Invalid option: $OPTARG requires an argument" 1>&2
  ;;
esac
done
projectionsArr=("")

if [ -z "$SEARCH_SPACE" ];then
  read -p "What view would you like to query? " SEARCH_SPACE;
fi
if [ -z "$projectionString" ];then
  if [[ $limitAttrs == "y" || $limitAttrs == "Y" ]]; then
    getAdsViewAttrsWithDocs "$SEARCH_SPACE"
    read -p "Enter the attributes to query separated by spaces: " projectionString;
    if [ -z "$projectionString" ]; then
      $projectionString="id"
    fi
    read -r projectionsArr <<< "$projectionString";
  else
    projectionsArr=$(getAdsViewAttrNames "$SEARCH_SPACE");
  fi
else
  read -r projectionsArr <<< "$projectionString";
fi

if [[ $useOptions == "Y" ]]; then
  optionsString="{";
  useOptions=true;
  if [[ $useFilter == "Y" ]]; then
    PS3="Select an attribute to filter on: "
    select projection in $projectionsArr; do
      filterAttr=$projection;
      break;
    done
    PS3="Select a filter operator: "
    select operator in "=" "=|i" "<>" "<>|i" ">" ">=" "<" "<=" "like" "like|i" "in" "in|i"; do
      filterOperator="\"$operator\"";
      break;
    done
    read -p "What is the filter value? " filterValue;
    optionsString+="\"filter\": [ $filterOperator, [\"~#ref\", \"$filterAttr\"], \"$filterValue\" ]"
  else
    optionsString+="\"filter\": []"
  fi

  if [[ $useSort == "Y" ]]; then
    PS3="Select an attribute to sort on: "
    select projection in $projectionsArr; do
      sortAttr=$projection;
      break;
    done
    select direction in "desc" "asc"; do
      sortDirection=$direction;
      break;
    done
    optionsString+=", \"sort\": [[[\"~#ref\", \"$sortAttr\"], \"$sortDirection\"]]"
  fi
  optionsString+=", \"limit\": $queryLimit}";
else
  useOptions=false;
  optionsString="null"
fi

FILENAME="/tmp/tempAdsGet.txt"
# Clear file
echo "" > $FILENAME;

echo "[[\"~#ref\", \"search_space\"], options_placeholder, {" >> $FILENAME
for proj in $projectionsArr
do
  projectionName=$(lookupAttrNameByUuid $proj)
  if [ -z "$projectionName" ];then
    projectionName=$proj
  fi
  echo "\"$projectionName\": [[\"~#ref\", \"$proj\"],null, null]," >> $FILENAME
done
#Remove the trailing comma
truncate -s -2 $FILENAME
printf '\n}]' >> $FILENAME
sed -i '.txt' "s/search_space/${SEARCH_SPACE#\ }/g" $FILENAME
sed -i '.txt' "s/options_placeholder/${optionsString}/g" $FILENAME
# show query executed
cat $FILENAME
ads-cli query -f $FILENAME
